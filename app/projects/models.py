from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save, m2m_changed

from profiles.models import Client, ClientContact
from django.contrib.auth.models import User
from django.core.files.uploadedfile import UploadedFile

from datetime import datetime
import os
from PIL import Image

def get_image_path(instance, filename):
    # print(instance.report.id)
    return os.path.join('reportes', str(instance.report.id), filename)

def get_thumbails_path(instance, filename):
    # print(instance.report.id)
    return os.path.join('thumbnails', filename)

class Project(models.Model):
    class Status(models.IntegerChoices):
        ACTIVO = 0, ("Activo")
        CIERRE_DOC = 1, ("Cierre Documental")
        PAUSADO = 2, ("Pausado")
        CERRADO = 3, ("Cerrado")
        CANCELADO = 4, ("Cancelado")
    
    class Types(models.IntegerChoices):
        PROY = 0, ("Proyecto")
        SUMI = 1, ("Suministro")
        SERV = 2, ("Servicio")
        FABR = 3, ("Fabricación")
        NOCO = 4, ("No contratado")

    cc_number = models.IntegerField(
        unique=True,
        null=True,
        blank=True,
        help_text="Numero de centro de costo"
    )
    name = models.CharField(
        max_length=255,
        help_text="Nombre del proyecto",
        null=True,
        blank=True,
    )
    type = models.IntegerField(
        choices=Types.choices,
        default=0,
        help_text="Tipo de proyecto",
        null=True,
    )
    date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        default = datetime.now,
        null=True,
        blank=True,
        help_text='Fecha de inicio'
    )
    end_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null = True,
        blank = True,
        help_text = "Fecha final del proyecto"
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.SET_NULL,
        related_name='%(class)s_client',
        null = True,
        blank = True,
        help_text = "Nombre del cliente"
    )
    contact = models.ManyToManyField(
        ClientContact,
        blank = True,
    )
    manager = models.ForeignKey(
        User,
        null = True,
        blank = True,
        on_delete=models.SET_NULL,
        help_text = "Es quien dara seguimiento al proyecto"
    )
    status = models.IntegerField(
        choices=Status.choices,
        default=0,
        help_text="Estado actual del proyecto",
        null=True
    )
    labour_cost = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        blank=True,
        null=True,
        help_text="Monto considerado para mano de obra del proyecto"
    )
    supply_cost = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        blank=True,
        null=True,
        help_text="Monto considerado para suministros del proyecto"
    )

    def __str__(self):
        return 'INX-CC-{}: {}'.format(self.cc_number, self.name)

class Activity(models.Model):
    class Status(models.IntegerChoices):
        NEW = 0, ('Nueva')
        IN_PROCESS = 1, ('En proceso')
        FINISHED = 2, ('Terminada')
        UNFINISHED = 3, ('Incompleta')
        CANCELED  = 4, ('Cancelada')
        
    class Types(models.IntegerChoices):
        ADDITIONAL = 0, ('Adicional')
        PROJECT = 1, ('Proyecto')
        ENGINEERING = 2, ('Ingenieria')
        SERVICE = 3, ('Servicio')
        OTHER  = 4, ('Otro')

    name = models.CharField(
        max_length= 50,
        help_text="Nombre corto de la actividad"
    )
    date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        default = datetime.now,
        null=True,
        blank=True,
        help_text='Fecha de inicio'
    )
    end_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null = True,
        blank = True,
        help_text = "Fecha estimada final de la actividada"
    )
    description = models.TextField(
        max_length=500,
        help_text="Descripcion adicional de la actividad si es necesario",
        null=True,
        blank=True,
    )
    type = models.IntegerField(
        choices=Types.choices,
        default = 0,
        help_text="Tipo de actividad",
    )
    status = models.IntegerField(
        choices=Status.choices,
        default = 0,
        help_text="Estado de la actividad"
    )
    project = models.ForeignKey(
        Project,
        on_delete = models.SET_NULL,
        null = True,
        blank = True,
        help_text = "Projecto al que pertenece")
    incharge = models.ForeignKey(
        User,
        null = True,
        blank = True,
        on_delete=models.SET_NULL,
        help_text = "Es el responsable de la actividad")

    def __str__(self):
        if self.project:
            return '[' + str(self.project.cc_number) + '] ' + str(self.project.name) + ': ' + str(self.name)
        else:
            return 'Sin proyecto: ' + self.name
        
    @receiver(post_save, sender=Project)
    def create_default_activity(sender, instance, created, **kwargs):
        if created:
            if instance.status == 0:
                act = Activity()
                act.name = "No planeadas CC-INX-" + str(instance.cc_number)
                act.description = instance
                act.project = instance
                act.save()

class ActivityProgress(models.Model):
    name = models.CharField(
        max_length=96,
        null=True,
        blank = True,
        help_text='Nombre de la medida de progreso'
    )
    amount = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
        help_text='Cantidad requerida',
    )
    unit = models.CharField(
        max_length=32,
        null=True,
        blank=True,
        help_text='Unidad de medida',
    )
    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        null=True,
        help_text='En que actividad se utiliza',
    )

class Requirements(models.Model):
    class status(models.IntegerChoices):
        PENDIENTE = 0, ('Pendiente')
        COMPLETO = 1, ('Completado')
        CANCEL = 2, ('Cancelado')

    name = models.CharField(
        max_length=50,
        help_text="Nombre corto del requerimiento"
    )
    request_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        default = datetime.now,
        null=True,
        blank=True,
        help_text='Fecha de solicitud'
    )
    supply_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        default = datetime.now,
        null=True,
        blank=True,
        help_text='Fecha de inicio'
    )
    by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='%(class)s_by',
        blank=True,
        null=True,
        help_text="Trabajador que solicita el requerimiento"
    )
    to = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='%(class)s_to',
        blank=True,
        null=True,
        help_text="Trabajador que debe entregar el requerimiento"
    )
    description=models.TextField(
        max_length=255,
        help_text="Descripcion de requerimiento",
    )
    amount=models.IntegerField(
        default=1,
        help_text="Cantidad de unidades del requerimiento"
    )
    unit = models.CharField(
        max_length=10,
        help_text="Unidad en las que se contabiliza el requerimiento"
    )
    activity = models.ForeignKey(
        Activity,
        on_delete=models.PROTECT,
        related_name='%(class)s_activities',
    )
    status = models.IntegerField(
        choices=status.choices,
        default = 0,
        help_text="Estado del requerimiento"
    )

    def __str__(self):
        return self.name

class Report(models.Model):
    class Deadtimetype(models.IntegerChoices):
        CLIENT = 0, ('A causa del cliente')
        MATERIAL = 1, ('Espera de materiales')
        WORKERS = 2, ('Espera de trabajadores')
        COMPLEX = 3, ('Tarea demaciado compleja')

    responsable = models.ForeignKey(
        User,
        related_name='%(class)s_responsable',
        on_delete=models.SET_NULL,
        null = True,
        blank = True,
        help_text = "Responsable de la actividad")
    date = models.DateField(
        default = datetime.now,
        help_text = "Fecha de la actividad AAAA-MM-DD"
    )
    name = models.CharField(
        max_length = 80,
        null=True,
        blank=True,
        help_text = "Nombre corto del reporte"
    )
    activity = models.ForeignKey(
        Activity,
        on_delete=models.SET_NULL,
        help_text="Seleccione la actividad",
        null=True,
        blank=True,
    )
    description = models.TextField(
        max_length=4096,
        help_text = "Descripción",
        null = True,
        blank = True)
    workers = models.ManyToManyField(
        User,
        related_name='%(class)s_workers',
        blank = True,
        help_text = "Ayudantes")
    duration = models.DecimalField(
        max_digits = 4,
        decimal_places = 2,
        null = True,
        blank = True,
        help_text = "Duración hora con punto decimal 1.5 = 1:30")
    deadtime = models.IntegerField(
        default=0,
        help_text="Tiempo muerto en minutos")
    deadtimereason = models.IntegerField(
        choices=Deadtimetype.choices,
        default=0,
        help_text="Razón por la cual se dio el tiempo muerto"
    )
    extratime = models.BooleanField(
        blank=True,
        null=True,
        help_text="Marcar si el reporte es en horas extras",
    )

    class meta:
        ordering = ['-date']

    def __str__(self):
        if self.name:
            return self.name
        else:
            return  self.date.strftime("%d/%m/%Y")

class UserTimeReport(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        help_text='A cual usuario hace referencia',
    )
    report = models.ForeignKey(
        Report,
        on_delete=models.CASCADE,
        null=True,
        help_text='A cual reporte hace referencia',
    )
    duration = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        default=0.0,
        blank=True,
        null=True,
        help_text='Cantidad de horas trabajadas',
    )

    '''
    Function to create a new UserTimeReport each time a new worker is added to a report
    '''
    @receiver(m2m_changed, sender=Report.workers.through)
    def create_default_activity(sender, instance, action, **kwargs):
        for user in instance.workers.all():
            try:
                utr = UserTimeReport.objects.get(user=user, report=instance)
            except:
                utr = UserTimeReport()
                utr.user = user
                utr.report = instance
                utr.duration = instance.duration
                utr.save()
        # print('action: {}\ninstance: {}\nkwargs: {}'.format(action, instance, kwargs))
        # if action == 'post_add':
        #     utr = UserTimeReport()
        #     utr.user = instance.workers.last()
        #     utr.report = instance
        #     utr.save()

class ReportProgress(models.Model):
    amount = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        default=0.0,
        blank=True,
        null=True,
        help_text='Cantidad avanzada',
    )
    activityprogress = models.ForeignKey(
        ActivityProgress,
        on_delete=models.CASCADE,
        null=True,
        help_text='A cual requerimiento hace referencia',
    )
    report = models.ForeignKey(
        Report,
        on_delete=models.CASCADE,
        null = True,
        help_text='Reporte en el cual se hizo el avance',
    )

class ReportMaterials(models.Model):
    report = models.ForeignKey(
        Report,
        on_delete=models.CASCADE,
        null = True,
        help_text='Reporte en el cual se utilizo el material',
    )
    description = models.TextField(
        max_length=4095,
        null = True,
        help_text="Descripcion del material",
    )
    amount = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null = True,
        help_text="Cantidad de unidades del material"
    )
    unit = models.CharField(
        max_length=10,
        null = True,
        help_text="Unidad en las que se contabiliza el material"
    )

class Photo(models.Model):
    report = models.ForeignKey(
        Report,
        on_delete=models.SET_NULL,
        null=True
    )
    pictures = models.FileField(
        upload_to=get_image_path,
        null = True,
        blank = True
    )
    thumbnail = models.FileField(
        upload_to=get_thumbails_path,
        null = True,
        blank = True
    )

