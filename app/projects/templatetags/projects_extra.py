import os
from django import template

register = template.Library()

@register.filter
def activityphoto(value):
    try:
        fullname = os.path.basename(value.file.name)
    except:
        fullname = 'No encontrado'
    short_name = fullname[:5] + '...' + fullname[-10:]
    return short_name