# Generated by Django 4.2.4 on 2023-09-14 11:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0013_alter_activity_type'),
    ]

    operations = [
        migrations.RenameField(
            model_name='activity',
            old_name='Type',
            new_name='type',
        ),
    ]
