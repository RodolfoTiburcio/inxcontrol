"""
This module contains Django views for managing projects, activities, and requirements.
"""

from typing import Any
from django.db import models
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import ( CreateView, ListView, DetailView, UpdateView, DeleteView,
                                   FormView, TemplateView, DayArchiveView, RedirectView )
from django.db.models import Max, Sum, Case, Value, When, F
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

from .forms import (ProjectCreateForm, ActivityCreateForm, RequirementCreateForm,
                    ReportCreateForm, PhotoForm, ActivityprogressForm, ReporprogressForm,
                    ProjectCostForm, ReportmaterialsForm, UserTimeReportAddForm)
from .models import (Project, Activity, Requirements, Report, Photo, ActivityProgress, ReportProgress,
                     ReportMaterials, UserTimeReport)

import os, datetime
from PIL import Image

from django.core.files.uploadedfile import UploadedFile

'''
Project views
'''

class ProjectCreateView(LoginRequiredMixin, CreateView):
    """
    View for creating a new project.
    """
    model = Project
    form_class = ProjectCreateForm
    
    def get_success_url(self) -> str:
        """
        Returns the URL to redirect to after successful project creation.
        """
        return reverse_lazy('projects:project_detail', kwargs={'pk':self.object.pk})

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the project creation form.
        """
        kwargs = super(ProjectCreateView, self).get_form_kwargs()
        u = self.request.user
        try:
            new_cc_number = Project.objects.all().aggregate(Max('cc_number'))['cc_number__max']+1
        except:
            new_cc_number = 1
        kwargs['initial'] = {
            'manager':u.pk,
            'cc_number':new_cc_number,
        }
        return kwargs
    
class ProjectListView(LoginRequiredMixin, ListView):
    """
    View for listing projects.
    """
    model = Project
    context_object_name = 'project_list'
    paginate_by = 20

    def get_queryset(self):
        """
        Returns the queryset of projects to be displayed.
        """
        queryset = super().get_queryset().order_by('-cc_number')
        if self.request.GET.get('status'):
            queryset = queryset.filter(
                status = self.request.GET.get('status')
            )
        return queryset

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        for p in context['project_list']:
            print(p)
            print(type(p))
            total_proy_progress = 0
            total_proy_requirement = 0
            for act in p.activity_set.all():
                total_activity_progress = 0
                total_activity_requirement = 0
                for prog in act.activityprogress_set.all():
                    suma = ReportProgress.objects.filter(
                        activityprogress=prog
                    ).aggregate(total_progress = Sum('amount'))
                    try:
                        percent = int(suma['total_progress']/prog.amount*100)
                    except:
                        percent = 0
                    total_activity_progress += percent
                    total_activity_requirement += 100
                if(total_activity_requirement):
                    total_activity_progress = int(total_activity_progress/total_activity_requirement*100)
                    total_proy_progress += total_activity_progress
                    total_proy_requirement += 100
            if(total_proy_requirement):
                total_proy_progress = int(total_proy_progress/total_proy_requirement*100)
                p.total_proy_progress = int(total_proy_progress)
        return context

class ProjectDetailView(LoginRequiredMixin, DetailView):
    """
    View for displaying project details.
    """
    model = Project
    context_object_name = 'project'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Returns the context data for rendering the project detail page.
        """
        context = super().get_context_data(**kwargs)
        p = context['object']
        activities = []
        total_proy_progress = 0
        total_proy_requirement = 0
        for act in p.activity_set.all():
            activities.append({
                'pk':act.pk,
                'name':act.name,
                'manager':act.incharge,
                'status':act.get_status_display(),
                'r':act.report_set.all().order_by('-date'),
                'reports':[],
            })
            for rep in act.report_set.all().order_by('-date'):
                activities[-1]['reports'].append({
                    'pk':rep.pk,
                    'date':rep.date,
                    'name':rep.name,
                    'description':rep.description,
                    'responsable':rep.responsable,
                    'progress':[],
                    'materials':[],
                })
                for material in rep.reportmaterials_set.all():
                    activities[-1]['reports'][-1]['materials'].append({
                        'description':material.description,
                        'amount':material.amount,
                        'unit':material.unit,
                    })
            
            total_activity_progress = 0
            total_activity_requirement = 0
            for p in act.activityprogress_set.all():
                suma = ReportProgress.objects.filter(
                    activityprogress=p
                ).aggregate(total_progress = Sum('amount'))
                try:
                    percent = int(suma['total_progress']/p.amount*100)
                except:
                    percent = 0
                total_activity_progress += percent
                total_activity_requirement += 100
            if(total_activity_requirement):
                total_activity_progress = int(total_activity_progress/total_activity_requirement*100)
                activities[-1]['total_activity_progress'] = total_activity_progress
                total_proy_progress += total_activity_progress
                total_proy_requirement += 100
        context['activities'] = activities
        if(total_proy_requirement):
            total_proy_progress = int(total_proy_progress/total_proy_requirement*100)
        context['total_proy_progress'] = total_proy_progress
        print(total_proy_progress)
        return context  

class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    """
    View for updating a project.
    """
    model = Project
    form_class = ProjectCreateForm
    success_url = reverse_lazy('projects:project_list')

    def get_success_url(self):
        """
        Returns the URL to redirect to after successful project update.
        """
        return reverse_lazy('projects:project_detail', kwargs = {'pk':self.kwargs['pk']})

class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    """
    View for deleting a project.
    """
    model = Project
    success_url = reverse_lazy('projects:project_list')
    context_object_name = 'project'

class ProjectCostUpdateView(LoginRequiredMixin, UpdateView):
    """
    View for updating the cost of a project.
    """
    model = Project
    form_class = ProjectCostForm

    def get_success_url(self):
        """
        Returns the URL to redirect to after successful project cost update.
        """
        return reverse_lazy('projects:project_detail', kwargs = {'pk':self.kwargs['pk']})

'''
Activity views
'''

class ActivityListView(LoginRequiredMixin, ListView):
    """
    View for listing activities.
    """
    model = Activity
    context_object_name = 'activity_list'
    paginate_by=10

    def get_queryset(self) -> QuerySet[Any]:
        """
        Returns the queryset of activities to be displayed.
        """
        if self.request.GET.get('type'):
            type_id = int(self.request.GET.get('type').strip('"'))
            queryset = Activity.objects.filter(
                type = type_id,
            ).order_by('-pk')
        elif self.request.GET.get('status') == '1':
            queryset = Activity.objects.filter(
                status__lte = 1
            ).order_by('-pk')
        elif self.request.GET.get('status') == '0':
            queryset = Activity.objects.all().order_by('-pk')
        else:
            queryset = Activity.objects.filter(
                status__lte = 1,
                incharge = self.request.user.pk
            ).order_by('-pk')
        return queryset
    
class ActivityPrintView(LoginRequiredMixin, TemplateView):
    """
    View for printing activities.
    """
    template_name = 'projects/activity_print.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Returns the context data for rendering the activity print page.
        """
        context = super().get_context_data(**kwargs)
        context['activity_list'] = [a for a in Activity.objects.all().order_by('project') if a.report_set.all()]
        return context

class ActivityCompactListView(LoginRequiredMixin, ListView):
    """
    View for listing activities in a compact format.
    """
    model = Activity
    paginate_by=20
    template_name = 'projects/activity_compact_list.html'

    def get_queryset(self) -> QuerySet[Any]:
        """
        Returns the queryset of activities to be displayed.
        """
        if self.request.GET.get('type'):
            type_id = int(self.request.GET.get('type').strip('"'))
            queryset = Activity.objects.filter(
                type = type_id,
            ).order_by('-pk')
        elif self.request.GET.get('status') == '1':
            queryset = Activity.objects.filter(
                status__lte = 1
            ).order_by('-pk')
        elif self.request.GET.get('status') == '0':
            queryset = Activity.objects.all().order_by('-pk')
        else:
            queryset = Activity.objects.filter(
                status__lte = 1,
                incharge = self.request.user.pk
            ).order_by('-pk')
        return queryset

class ActivityWorkingListView(LoginRequiredMixin, ListView):
    """
    View for listing activities in a compact format. (gropued by project)
    """
    model = Project
    template_name = 'projects/activity_working_list.html'
    context_object_name = 'project_list'

    def get_queryset(self) -> QuerySet[Any]:
        """
        Return the queryset of projects with activities in progress.
        """
        queryset = Project.objects.filter(
            activity__status__lte = 1,
        ).distinct()
        for p in queryset:
            p.activities = Activity.objects.filter(
                project = p,
                status__lte = 1,
            )
        return queryset
    
class ActivityCreateView(LoginRequiredMixin, CreateView):
    """
    View for creating a new activity.
    """
    model = Activity
    form_class = ActivityCreateForm

    def get_success_url(self):
        """
        Returns the URL to redirect to after successful activity creation.
        """
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.pk})

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the activity creation form.
        """
        kwargs = super(ActivityCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'incharge':u.pk,
        }
        project_pk = self.request.GET.get('project')
        if project_pk:
            kwargs['initial']['project'] = project_pk
        return kwargs

class ActivityDetailView(LoginRequiredMixin, DetailView):
    """
    View for displaying activity details.
    """
    model = Activity
    context_object_name = 'activity'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Returns the context data for rendering the activity detail page.
        """
        context = super().get_context_data(**kwargs)
        act = context['object']
        progress = []
        if self.request.GET.get('terminar'):
            if self.request.GET.get('terminar') == 'True' and act.status != 2:
                act.status = 2
                act.save()
        total_activity_progress = 0
        total_activity_requirement = 0
        for p in act.activityprogress_set.all():
            suma = ReportProgress.objects.filter(
                activityprogress=p
            ).aggregate(total_progress = Sum('amount'))
            try:
                percent = int(suma['total_progress']/p.amount*100)
            except:
                percent = 0
            progress.append({
                'id':p.pk,
                'name':p.name,
                'total':suma['total_progress'],
                'goal':p.amount,
                'unit':p.unit,
                'percent': percent,
            })
            total_activity_progress += percent
            total_activity_requirement += 100
        if(total_activity_requirement):
            total_activity_progress = int(total_activity_progress/total_activity_requirement*100)
            total_activity_requirement = 100
        # print(total_activity_progress)
        # print(total_activity_requirement)
        context['reports_by_date'] = act.report_set.all().order_by('-date')
        context['progress']=progress
        context['total_activity_progress'] = total_activity_progress
        return context

class ActivityUpdateView(LoginRequiredMixin, UpdateView):
    """
    View for updating an activity.
    """
    model = Activity
    form_class = ActivityCreateForm
    success_url = reverse_lazy('projects:activity_list')

    def get_success_url(self) -> str:
        """
        Returns the URL to redirect to after successful activity update.
        """
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.kwargs['pk']})

class ActivityDeleteView(LoginRequiredMixin, DeleteView):
    """
    View for deleting an activity.
    """
    model = Activity
    success_url = reverse_lazy('projects:activity_list')
    context_object_name = 'activity'

class ActivityReportView(LoginRequiredMixin, TemplateView):
    """
    View for generating activity reports.
    """
    template_name = 'projects/activity_report.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Returns the context data for rendering the activity report page.
        """
        context = super().get_context_data(**kwargs)
        activities = Activity.objects.filter(status__lte=1)
        # annotate the queryset with the days between the last report and today
        activities = activities.annotate(
            last_report = Max('report__date')
        )
        test_activities = activities.annotate(
            days_since_last_report = Case(
                When(
                    last_report__isnull = True,
                    then = datetime.timedelta(days=0)
                ),
                default = (datetime.date.today() - F('last_report') )
            )
        ).order_by('-days_since_last_report')
        context['activity_list'] = test_activities
        return context

class ActivityProgressCreateView(LoginRequiredMixin, CreateView):
    model = ActivityProgress
    form_class = ActivityprogressForm

    def get_initial(self):
        initial = super().get_initial()
        initial['activity'] = self.kwargs.get("pk")
        return initial

    def get_success_url(self):
        return self.request.GET.get('next') or reverse_lazy('projects:activity_detail', kwargs={'pk':self.kwargs['pk']})

class ActivityProgressUpdateView(LoginRequiredMixin, UpdateView):
    model = ActivityProgress
    form_class = ActivityprogressForm

    def get_success_url(self):
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.activity.pk})

class ActivityProgressDeleteView(LoginRequiredMixin, DeleteView):
    model = ActivityProgress
    context_object_name = 'activityprogress'

    def get_success_url(self):
        return reverse_lazy('projects:activity_detail', kwargs={'pk':self.object.activity.pk})

'''
Requirement views
'''

class RequirementListView(LoginRequiredMixin, ListView):
    """
    View for listing requirements.
    """
    model = Requirements
    context_object_name = 'requirements_list'
    # paginate_by=20

    def get_queryset(self) -> QuerySet[Any]:
        """
        Returns the queryset of requirements to be displayed.
        """
        raw_status = self.request.GET.get('status')
        if raw_status:
            status = int(raw_status)
            queryset = Requirements.objects.filter(status = status)
        elif self.request.GET.get('mios'):
            queryset = Requirements.objects.filter(
                to = self.request.user.pk
            )
        else:
            queryset = Requirements.objects.all()
        return queryset

class RequirementCreateView(LoginRequiredMixin, CreateView):
    """
    View for creating a new requirement.
    """
    model = Requirements
    form_class = RequirementCreateForm
    success_url = reverse_lazy('projects:requirement_list')

    def get_form_kwargs(self):
        kwargs = super(RequirementCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'by':u.pk,
            'to':7,
            'supply_date':datetime.datetime.now() + datetime.timedelta(days=2)
        }
        return kwargs
    
class RequirementUpdateView(LoginRequiredMixin, UpdateView):
    model = Requirements
    form_class = RequirementCreateForm

    def get_success_url(self) -> str:
        return '{}?status=0'.format(reverse_lazy('projects:requirement_list'))

class RequirementCompleteView(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        request_to_complete = self.kwargs['pk']
        try:
            req = Requirements.objects.get(pk=request_to_complete)
            req.status = 1
            req.save()
        except:
            print('no se pudo acutalizar')
        url = '{}?status=0'.format(
            reverse_lazy('projects:requirement_list')
        )
        return url

'''
Report views
'''

class ReportCardListView(LoginRequiredMixin, ListView):
    model = Report
    context_object_name = 'report_list'
    paginate_by = 10
    ordering = '-date'
    template_name = 'projects/report_card_list.html'

class ReportListView(LoginRequiredMixin, ListView):
    model = Report
    context_object_name = 'report_list'
    paginate_by = 10
    #ordering = '-date'

    def get_queryset(self):
        # print(self.request.GET.get('mios'))
        if self.request.GET.get('mios'):
            u = self.request.user
            queryset = Report.objects.filter(responsable=u).order_by('-date')
        elif self.request.GET.get('noact'):
            queryset = Report.objects.filter(activity=None)
        elif self.request.GET.get('adicional'):
            queryset = Report.objects.filter(activity__type = 0, activity__status__lte = 1).order_by('activity')
        elif self.request.GET.get('oficina'):
            users = User.objects.filter(groups__name = 'Oficina')
            #print(users)
            queryset = Report.objects.filter(responsable__in=users).order_by('-date')
        else:
            queryset = Report.objects.all().order_by('-date')
        return queryset

class ReportCreateView(LoginRequiredMixin, CreateView):
    model = Report
    form_class = ReportCreateForm

    def get_form_kwargs(self):
        kwargs = super(ReportCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'responsable':u.pk,
        }
        activity = self.request.GET.get('activity')
        if activity:
            kwargs['initial']['activity']=activity
        return kwargs

    def get_success_url(self):
        try:
            if self.object.activity.activityprogress_set.all():
                return reverse_lazy('projects:report2_create', kwargs = {'pk':self.object.pk})
            return reverse_lazy('projects:report_detail', kwargs = {'pk':self.object.pk})
        except:
            return reverse_lazy('projects:report_detail', kwargs = {'pk':self.object.pk})

class ReportUpdateView(LoginRequiredMixin, UpdateView):
    model = Report
    form_class = ReportCreateForm
    success_url = reverse_lazy('projects:report_list')

    def get_success_url(self) -> str:
        return reverse_lazy('projects:report_detail', kwargs={'pk': self.kwargs['pk']})

class ReportDetailView(LoginRequiredMixin, DetailView):
    model = Report
    context_object_name = 'report'

class ReportDeleteView(LoginRequiredMixin, DeleteView):
    model = Report
    context_object_name = 'report'
    success_url = reverse_lazy('projects:report_list')

class PhotoCreateView(LoginRequiredMixin, FormView):
    form_class = PhotoForm
    template_name='projects/photo_form.html'

    def get_success_url(self):
        return self.request.GET.get('next') or reverse_lazy('projects:report_detail', kwargs={'pk':self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report'] = Report.objects.get(pk=self.kwargs.get("pk"))
        context['photos'] = Photo.objects.filter(report = self.kwargs.get("pk"))
        all_time_report = UserTimeReport.objects.all()
        # print(all_time_report)
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['report'] = self.kwargs.get("pk")
        return initial
        
    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        files = form.cleaned_data["pictures"]
        report = Report.objects.get(pk=int(self.kwargs.get("pk")))
        THUMBNAIL_SIZE = (80, 80)
        PIL_TYPE = ''
        for f in files:
            p = Photo.objects.create(report = report, pictures=f)
            DJANGO_TYPE = f.content_type
            if DJANGO_TYPE == 'image/jpeg':
                PIL_TYPE = 'jpeg'
            elif DJANGO_TYPE == 'image/png':
                PIL_TYPE = 'png'
            if PIL_TYPE in ['jpeg', 'png']:
                with p.pictures.open() as original:
                    image = Image.open(original)
                    image.thumbnail(THUMBNAIL_SIZE)
                    image.save('temp', format = PIL_TYPE)
                    with open('temp', 'rb') as thum:
                        p.thumbnail.save(
                            original.name,
                            UploadedFile(
                                file = thum,
                                content_type=DJANGO_TYPE
                            )
                        )
        return super().form_valid(form)

class ReportDayArchiveView(LoginRequiredMixin, DayArchiveView):
    model = Report
    date_field = 'date'
    day_format = '%d'
    context_object_name = 'reports'
    allow_empty = True
    ordering = 'activity'

class ReportLastdayArchiveView(LoginRequiredMixin, RedirectView):
    year = datetime.date.today().year
    month = datetime.date.today().strftime('%b')
    day = datetime.date.today().day
    try:
        ultima_fecha = Report.objects.all().order_by('-date')[0].date
        #print(ultima_fecha)
    except:
        ultima_fecha = None
    queryset = Report.objects.filter(date = ultima_fecha).order_by('activity')
    url = reverse_lazy(
        'projects:report_dayarchive',
        kwargs = {
            'year':year,
            'month':month,
            'day':day,
        }
    )

class UserTimeReportUpdateView(LoginRequiredMixin, UpdateView):
    model = UserTimeReport
    fields = ['duration']

    def get_success_url(self):
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.object.report.pk})
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['report'] = Report.objects.get(pk=self.object.report.pk)
        context['photos'] = Photo.objects.filter(report = self.object.report.pk)
        context['current_utr'] = self.object.pk
        return context
    
class UserTimeReportConvertView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        report_to_get_users = self.kwargs['pk']
        try:
            rep = Report.objects.get(pk=report_to_get_users)
        except:
            print('Reporte no encontrado')
        if rep:
            for u in rep.workers.all():
                try:
                    utr = UserTimeReport.objects.get(report=rep, user=u)
                except:
                    utr = UserTimeReport.objects.create(
                        report=rep,
                        user=u,
                        duration=rep.duration
                    )
        return reverse_lazy('projects:report_detail', kwargs={'pk':rep.pk})

class UserTimeReportAddView(LoginRequiredMixin, FormView):
    form_class = UserTimeReportAddForm
    template_name = 'projects/usertimereport_add.html'

    def get_success_url(self):
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.kwargs['pk']})
    
    # def get_form_kwargs(self) -> dict[str, Any]:
    #     kwargs = super(UserTimeReportAddView, self).get_form_kwargs()
    #     kwargs['initial'] = {
    #         'report':self.kwargs['pk']
    #     }
    #     return kwargs

    
    def form_valid(self, form):
        report = Report.objects.get(pk=self.kwargs['pk'])
        for u in form.cleaned_data['workers']:
            try:
                utr = UserTimeReport.objects.get(report=report, user=u)
            except:
                utr = UserTimeReport.objects.create(
                    report=report,
                    user=u,
                    duration=report.duration
                )
        return super().form_valid(form)

class ReportProgressCreateView(LoginRequiredMixin, FormView):
    form_class = ReporprogressForm
    template_name = 'projects/report2_form.html'
    success_url = reverse_lazy("projects:report_list")

    def get_success_url(self) -> str:
        return reverse_lazy("projects:report_detail", kwargs={'pk':self.kwargs['pk']})

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super(ReportProgressCreateView, self).get_form_kwargs()
        kwargs['initial'] = {
            'act':Report.objects.get(
                pk = self.kwargs['pk'],
            ).activity.pk
            }
        return kwargs

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
        
    def form_valid(self, form):
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        progress_fields = ActivityProgress.objects.filter(
            activity = Report.objects.get(
                pk = self.kwargs['pk']
            ).activity
        )
        for progress_field in progress_fields:
            if(form.data[progress_field.name]):
                #print('val: {}'.format(form.data[progress_field.name]))
                r = ReportProgress.objects.create(
                    activityprogress = progress_field,
                    report = Report.objects.get(pk = self.kwargs['pk']),
                    amount = float(form.data[progress_field.name])
                )
        return super().form_valid(form)

class ReportProgressUpdateView(LoginRequiredMixin, UpdateView):
    model = ReportProgress
    fields = ['amount']
    template_name = 'projects/report2_form.html'

    def get_success_url(self):
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.object.report.pk})
    
class ReportProgressDeleteView(LoginRequiredMixin, DeleteView):
    model = ReportProgress
    context_object_name = 'reportprogress'


    def get_success_url(self):
        #print(self.object.report.pk)
        return reverse_lazy('projects:report_detail', kwargs={'pk':self.object.report.pk})

class ReportMaterialsCreateView(LoginRequiredMixin, CreateView):
    model = ReportMaterials
    fields = '__all__'

    def get_initial(self):
        initial = super().get_initial()
        initial['report'] = self.kwargs.get("pk")
        return initial

    def get_success_url(self):
        return self.request.GET.get('next') or reverse_lazy('projects:reportmaterial_create', kwargs={'pk':self.kwargs['pk']})
    
    # def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
    #     print("Hola")
    #     return super().post(request, *args, **kwargs)

    # def form_valid(self, form):
    #     return super().form_valid(form)
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['report'] = Report.objects.get(pk=self.kwargs.get("pk"))
        context['materials'] = ReportMaterials.objects.filter(report = self.kwargs.get("pk"))
        return context
    
class ReportMaterialsDeleteView(LoginRequiredMixin, DeleteView):
    model = ReportMaterials
    context_object_name = 'reportmaterials'

    def get_success_url(self):
        return reverse_lazy('projects:reportmaterial_create', kwargs={'pk':self.object.report.pk})
    
class ReportListPrint(LoginRequiredMixin, TemplateView):
    template_name = 'projects/report_list_print.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['reports'] = Report.objects.filter(activity__client__ok=3).order_by('-date')
        return context
    
class UserReportListView(LoginRequiredMixin, ListView):
    model = Report
    context_object_name = 'report_list'
    template_name = 'projects/user_report_list.html'

    def get_queryset(self):
        queryset = User.objects.get(
            pk=self.kwargs['pk']
        ).report_responsable.all().filter(
            activity__type = 0,
        ).order_by('-date')
        return queryset

    # def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
    #     context = super().get_context_data(**kwargs)
    #     context['additionals'] = []
    #     for r in context['report_list']:
    #         if r.activity.type == 0:
    #             context['additionals'].append(r)

'''
Tools views
'''

class SearchListView(LoginRequiredMixin, TemplateView):
    template_name = 'search_results.html'

    def post(self, request, *args, **kwargs):
        searched = request.POST['valuesearch']
        context = {}
        context['searched'] =  searched
        context['projects'] = Project.objects.filter(name__icontains = searched)
        context['activities'] = Activity.objects.filter(name__icontains = searched)
        context['reports'] = Report.objects.filter(description__icontains = searched)
        return render(
            template_name='search_results.html',
            request=request,
            context=context
        )
  