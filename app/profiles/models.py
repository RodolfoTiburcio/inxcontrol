from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime

class Contact(models.Model):
    """
    Modelo para datos de contactos que no son usuarios
    """
    name = models.CharField(
        max_length=127,
        help_text="Nombre del contacto",
    )
    phone = models.CharField(
        max_length=16,
        help_text="Telefono del contacto",
        blank=True,
        null = True,
    )
    notes = models.TextField(
        max_length=511,
        help_text="Notas del contacto",
        blank=True,
        null = True,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class Client(models.Model):
    """
    Modelo para los clientes
    """
    class Status(models.IntegerChoices):
        ACTIVO = 0, ("Activo")
        INACTIVO = 1, ("Inactivo")

    name = models.CharField(
        max_length=127,
        help_text="Nombre del cliente"
    )
    status = models.IntegerField(
        choices=Status.choices,
        default=0,
        help_text="Estado actual del cliente"
    )
    score = models.IntegerField(
        default=10,
        help_text="Calificación del cliente"
    )

    def __str__(self):
        return f"{self.name}"
    
class ClientContact(Contact):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        help_text="Cliente asignado"
    )
    
class Supplier(models.Model):
    """
    Modelo para los proveedores, gregresar orden por categoria
    """
    name = models.CharField(
        max_length=127,
        help_text="Nombre del proveedor"
    )
    category = models.ForeignKey(
        'SupplierCategory',
        on_delete=models.CASCADE,
        null = True,
        blank = True,
        help_text="Categoria del proveedor"
    )

    def __str__(self):
        return '{}: {}'.format(self.category, self.name)
    
    class Meta:
        ordering = ['category']

class SupplierContact(Contact):
    supplier = models.ForeignKey(
        Supplier,
        on_delete=models.CASCADE,
        help_text="Proveedor asignado"
    )

class SupplierCategory(models.Model):
    name = models.CharField(
        max_length=127,
        help_text="Nombre de la categoria"
    )

    def __str__(self):
        return self.name

class WorkersTime(models.Model):
    workdate = models.DateField(
        default = datetime.now,
        blank=True,
        null = True,
    )
    worker = models.ManyToManyField(
        User,
        help_text= "Trabajador asignado"
    )
    client = models.ForeignKey(
        Client, 
        on_delete = models.CASCADE,
        help_text = "Cliente al cual esta asignado"
    )

class WorkerCost(models.Model):
    worker = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True
    )
    normal_cost = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True,
        help_text="Costo normal por hora"
    )
    extra_cost = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True,
        help_text="Costo extra por hora"
    )