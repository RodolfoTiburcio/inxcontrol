# Generated by Django 4.2.4 on 2024-01-06 19:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0011_supplier_category'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='supplier',
            options={'ordering': ['category']},
        ),
    ]
