from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('inxcontrol/admin/', admin.site.urls),
    path('inxcontrol/profiles/', include('profiles.urls'), name='profiles'),
    path('inxcontrol/finances/', include('finances.urls'), name='finances'),
    path('inxcontrol/projects/', include('projects.urls'), name='projects'),
    path('inxcontrol/sales/', include('sales.urls'), name='sales'),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)