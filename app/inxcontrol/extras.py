from projects.models import Photo
from PIL import Image
from io import StringIO
import os
import csv
from datetime import datetime
from sales.models import Opportunity
from profiles.models import Client, ClientContact
from django.contrib.auth.models import User
from django.db import IntegrityError

from django.core.files.uploadedfile import UploadedFile
# Run this program with: exec(open("./inxcontrol/extras.py").read())
# in a container django shell


def create_users():
    '''
    Load data from "Export_User.csv" file into User model
    '''
    with open('inxcontrol/Export_User.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        # create a list of dictionaries from the csv file
        users = list(reader)
        for u in users:
            temp_user = User.objects.filter(
                first_name__iexact=u['firstName'],
                last_name=u['lastName']
            )
            if len(temp_user) == 0:
                try:
                    user = User.objects.create_user(
                        username=u['id'],
                        first_name=u['firstName'],
                        last_name=u['lastName'],
                    )
                except IntegrityError:
                    print(f'Error: {u["username"]} already exists')

def create_clients():
    '''
    Load
    '''

def create_oppotunities():
    '''
    load data from "Export_Opportunity.csv" file into Opportunity model
    the file must be in the same folder as this script
    the header of the file must be:
    id;name;amount;amountWeightedConverted;stage;lastStage;probability;leadSource;closeDate;description;contactRole;createdAt;modifiedAt;concecutivo;ready;documentosNecesarios;epprequerido;areatrabajo;equipoAIntervenir;libranza;finesdesemana;tiempoRequerido;supervisor;electrico;soldadorPailero;ayudante;otros;comentarios;materialesNecesarios;fechaDeNegociacin;amountCurrency;amountConverted;accountId;accountName;contactsColumns;contactId;contactName;campaignId;campaignName;originalLeadId;originalLeadName;createdById;createdByName;modifiedById;modifiedByName;assignedUserId;assignedUserName;fotos2Types
    '''
    with open('inxcontrol/Export_Opportunity.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        # create a list of dictionaries from the csv file
        opps = list(reader)
        print(opps[0])

        for row in opps:
            try:
                client = Client.objects.get(name__iexact=row['accountName'])
            except Client.DoesNotExist:
                client = Client.objects.create(name=row['accountName'])
            # try:
            #     contact = ClientContact.objects.get(name=row['contactName'])
            # except ClientContact.DoesNotExist:
            #     contact = ClientContact.objects.create(name=row['contactName'])
            try:
                responsable = User.objects.get(username=row['createdByName'])
            except User.DoesNotExist:
                responsable = User.objects.get(username='admin')
            try:
                opportunity = Opportunity.objects.create(
                    name=row['name'],
                    amount=row['amount'],
                    status=row['stage'],
                    open_date=datetime.strptime(row['createdAt'], '%Y-%m-%d %H:%M:%S'),
                    client=client,
                    # contact=contact,
                    description=row['description'],
                    win_probability=row['probability'],
                    responsable=responsable,
                )
            except IntegrityError:
                print(f'Error: {row["name"]} already exists')


def create_thumbnails():
    THUMBNAIL_SIZE = (99, 66)
    PIL_TYPE = ''
    DJANGO_TYPE = ''
    photos = Photo.objects.all()

    for photo in photos:
        a, b = os.path.splitext(photo.pictures.path)
        file_extension = b[1:]
        if file_extension == 'jpg':
            PIL_TYPE = 'jpeg'
            DJANGO_TYPE = 'image/jpeg'
        elif file_extension == 'png':
            PIL_TYPE = 'png'
            DJANGO_TYPE = 'image/png'
        if file_extension in ['jpg', 'png']:
            with photo.pictures.open() as original:
                #print(original)
                image = Image.open(original)
                image.thumbnail(THUMBNAIL_SIZE)
                temp_handle = StringIO()
                image.save('temp', format = PIL_TYPE)
                with open('temp', 'rb') as thum:
                    photo.thumbnail.save(
                        original.name,
                        UploadedFile(
                            file = thum,
                            content_type=DJANGO_TYPE
                        )
                    )

def bu_database():
    pass

if __name__ == 'builtins':
    # create_users()
    create_oppotunities()

