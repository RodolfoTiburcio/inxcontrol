from redminelib import Redmine
from datetime import datetime
import pandas as pd
redmine = Redmine('http://t.inamexa.com:8082', username = 'adrian.islas@inamexa.com', password='testpython983742378659384')
# project = redmine.project.get('cc-inx-148-remoto-ad-visualizacion-basc-int-delta-p')
# print(issue)
#attachment = issue.attachments[0]
#filepath = attachment.download(savepath='../mediafiles/redmine/', filename='image.jpg')
#print(filepath)
##### SALA LEAN ######
issues_ids = [317, 313, 309, 271]
##### Mantenimiento a ccm ######
# issues_ids = [325]
##### Desmonte en vias
# issues_ids = [327, 287, 261]
##### Levantamiento 
# cc = 322
# Otros = [329, 319, 326, 318, 324, 323, 308, 312, 303, 315]
# for issue_id in issues_ids:
#     time_entrtys.append(redmine.time_entry.filter(issue_id=issue_id))
# print(time_entrtys[0][0])
issues_reg = [
    {'issue_id':328,'project_cc':228, 'Actividad':'Dibujos en CAD'}, # Maquina de goteo cad
    {'issue_id':327,'project_cc':000, 'Actividad':'Desmonte de vias'}, # Desmonte de vias (adicional)
    {'issue_id':326,'project_cc':303, 'Actividad':'Instalacion de bases'}, # Camaras
    {'issue_id':325,'project_cc':310, 'Actividad':'Mantenimiento Civil'}, # Mantenimiento a CCM
    {'issue_id':324,'project_cc':303, 'Actividad':'Instalacion de bases'}, # camaras
    {'issue_id':323,'project_cc':303, 'Actividad':'Instalacion de bases'}, # Camaras
    {'issue_id':322,'project_cc':228, 'Actividad':'Integrar libro de proyecto'}, # MAquina de goteo
    {'issue_id':321,'project_cc':315, 'Actividad':'Integrar libro de proyecto'}, # Temperadora de grasa
    {'issue_id':320,'project_cc':319, 'Actividad':'Integrar libro de proyecto'}, # Tanques cobertura blanca
    {'issue_id':319,'project_cc':315, 'Actividad':'Dibujos en CAD'}, # Temperadora de grasa
    {'issue_id':318,'project_cc':319, 'Actividad':'Dibujos en CAD'}, # Tanques cobertura blanca
    {'issue_id':317,'project_cc':329, 'Actividad':'Trabajo civil y cambio de tuberia'}, # Sala LEAN
    {'issue_id':316,'project_cc':000, 'Actividad':'Pintura de franja verde'}, # Adicionales Dean (adicional)
    {'issue_id':315,'project_cc':000, 'Actividad':'Mantenimiento a clima'}, # Adicional Carlos (adicional)
    {'issue_id':314,'project_cc':312, 'Actividad':'Fabricación en taller de Cargill'}, # Puertas retractiles
    {'issue_id':313,'project_cc':329, 'Actividad':'Remodelación sala lean'}, # Sala LEAN
    {'issue_id':312,'project_cc':131, 'Actividad':'Instalación y puesta en marcha en TM2'}, # CC131 Drive TM
    {'issue_id':311,'project_cc':267, 'Actividad':'Fabricación en taller de Cargill'}, # Caseta bascula (Postes)
    {'issue_id':310,'project_cc':328, 'Actividad':'Fabricación en taller de Cargill'}, # Soldadura base ventilador
    {'issue_id':309,'project_cc':329, 'Actividad':'Remodelación sala lean'}, # Sala LEAN
    {'issue_id':308,'project_cc':228, 'Actividad':'Peinado y recopilación de datos en campo.'}, # Maquina de goteo, as build
    {'issue_id':307,'project_cc':228, 'Actividad':'Puesta en marcha del sistema'}, # Maquina de goteo, Pruebas
    {'issue_id':306,'project_cc':267, 'Actividad':'Fabricación en taller de Cargill'}, # Caseta de bascula, soporte alumbrado (adicional)
    {'issue_id':305,'project_cc':131, 'Actividad':'Fabricación en taller de Cargill'}, # CC131 Drvie TM, Base para variador
    {'issue_id':304,'project_cc':000, 'Actividad':'Reparacion puertas de talud'},   # Reparacion puertas de talud (adicional)
]
reports = []
journals = []
for issue_id in issues_ids:
    issue = redmine.issue.get(issue_id)
    for timeentry in issue.time_entries:
        reports.append({
            'responsable':timeentry.user['name'], # despues de probar cambiar a id
            'tipo_de_actividad':timeentry.activity['name'],
            'tiempo_dedicado':timeentry.hours,
            'descripcion':timeentry.comments,
            'fecha':timeentry.spent_on,
            'workers':timeentry.custom_fields[0]['value'],
            'deadtime':timeentry.custom_fields[2]['value']
        })
    for journal in issue.journals:
        journals.append({
            'responsable':journal.user['name'],
            'comment':journal.notes,
            'fecha':journal.created_on.date(),
            'fotos':journal.details
        })

reports_df = pd.DataFrame(reports)
journals_df = pd.DataFrame(journals)

final_reports = reports_df.merge(journals_df, on=['responsable', 'fecha'])
final_reports.to_pickle('./final_reports.pkl')
reports_df.to_pickle('./reports_df.pkl')
journals_df.to_pickle('./journals_df.pkl')

# print(reports)
# print(journals)
# for each in issue.time_entries:
#     print(each)

# timeentry = redmine.time_entry.get(294)

# print(report)
# test_time_entry = [
#     ('id', 294), # referencia
#     ('project', {'id': 31, 'name': 'REPORTE DIARIO SCC-INX-SEPTIEMBRE-2023'}), #No importa
#     ('issue', {'id': 317}), #referencia
#     ('user', {'id': 10, 'name': 'Raymundo O.'}), #responsable
#     ('activity', {'id': 11, 'name': 'Proyecto'}), # Tipo de actividad
#     ('hours', 8.0), # Tiempo dedicado
#     ('comments', 'La actividad comienza tarde por falta de escalera de extensión. La actividad paró antes por orden del director. '), # Descripcion
#     ('spent_on', '2023-09-09'), # Fecha
#     ('created_on', '2023-09-09T21:07:33Z'),
#     ('updated_on', '2023-09-09T21:07:33Z'),
#     ('custom_fields', [
#         {
#             'id': 2,  # Workers
#             'name': 'Personal',
#             'multiple': True,
#             'value': [
#                 'JGEV - JESUS G ESCAMILLA V',
#                 'JMC  - JOSE MIGUEL CERVANTES'
#             ]
#         },
#         { # Cliente no importa esta en el proyecto
#             'id': 3,
#             'name':
#             'Cliente',
#             'multiple': True,
#             'value': ['14']
#         },
#         { # Tiempo muerto
#             'id': 5,
#             'name': 'Tiempo muerto (min)',
#             'value': '240'
#         }
#     ])
# ]

# test_journal = [
#     ('id', 856),
#     ('user', {'id': 10, 'name': 'Raymundo O.'}),
#     ('notes', 'Se retiró la tubería de 1".\r\nSe tuvo que esperar a que se desocupara la escalera para empezar la actividad  (11am). Mientras tanto el personal asignado estuvo ayudando dentro de la sala lean.\r\n\r\nSe punto el punto de anclaje desde la azotea, se hace un hueco en el barandal para cambiar tubería de pvc de 3"\r\n\r\nPosterior se empieza a picar el concreto para fabricar un chaflán en la orilla de la sala... la actividad paró con 1m de avance por el ruido. '),
#     ('created_on', '2023-09-09T21:07:33Z'),
#     ('private_notes', False),
#     ('details', [
#         {
#             'property': 'attachment',
#             'name': '448',
#             'old_value': None,
#             'new_value': 'Screenshot_20230909-145048_Photos.jpg'
#         },
#         {
#             'property': 'attachment',
#             'name': '447',
#             'old_value': None,
#             'new_value': 'Screenshot_20230909-145104_Photos~2.jpg'
#         },
#         {
#             'property': 'attachment',
#             'name': '449',
#             'old_value': None,
#             'new_value': 'Screenshot_20230909-145152_Photos.jpg'
#         }
#     ])
# ]

# [
#     {
#         'responsable': 'Raymundo O.',
#         'tipo_de_actividad': 'Proyecto',
#         'tiempo_dedicado': 8.0, 'descripcion':
#         'La actividad comienza tarde por falta de escalera de extensión. La actividad paró antes por orden del director. ',
#         'fecha': datetime.date(2023, 9, 9),
#         'workers': ['JGEV - JESUS G ESCAMILLA V', 'JMC  - JOSE MIGUEL CERVANTES'],
#         'deadtime': '240'
#     }
# ]
# [
#     {
#         'responsable': 'Raymundo O.',
#         'descripcion': 'Se retiró la tubería de 1".\r\nSe tuvo que esperar a que se desocupara la escalera para empezar la actividad  (11am). Mientras tanto el personal asignado estuvo ayudando dentro de la sala lean.\r\n\r\nSe punto el punto de anclaje desde la azotea, se hace un hueco en el barandal para cambiar tubería de pvc de 3"\r\n\r\nPosterior se empieza a picar el concreto para fabricar un chaflán en la orilla de la sala... la actividad paró con 1m de avance por el ruido. ',
#         'fecha': '2023-09-09',
#         'fotos': [
#             {
#                 'property': 'attachment',
#                 'name': '448',
#                 'old_value': None,
#                 'new_value': 'Screenshot_20230909-145048_Photos.jpg'
#             },
#             {
#                 'property': 'attachment',
#                 'name': '447',
#                 'old_value': None,
#                 'new_value': 'Screenshot_20230909-145104_Photos~2.jpg'
#             },
#             {
#                 'property': 'attachment',
#                 'name': '449',
#                 'old_value': None,
#                 'new_value': 'Screenshot_20230909-145152_Photos.jpg'
#             }
#         ]
#     }
# ]