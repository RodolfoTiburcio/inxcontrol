import pandas as pd
from django.contrib.auth.models import User, Group
from profiles.models import Client
from projects.models import Project, Activity
from datetime import datetime
df = pd.read_csv('./inxcontrol/Proyectos-TODO.csv')

# Run this program with: exec(open("./inxcontrol/projects_analisys.py").read())
# in a container django shell

tipos = {
    'PROYECTO':0,
    'SUMINISTRO':1,
    'SERVICIO':2,
    'FABRICACION':3,
    'NO-CONTRATADO':4,
}
estados = {
    'Activo':0,
    'Cierre Documental':1,
    'Pausado':2,
    'Cerrado':3,
    'Cancelado':4,
}
usuarios = {
    'Rodolfo Tiburcio':1,
    'Nohemi Bautista Perez':8,
    'presupuestos@inamexa.cim':9,
    'NELSON OBED LARA COLORADO':10,
    'Rafael Aguirre Nolasco':2,
}
current_users = {
    1:{
        'first_name':'Rodolfo',
        'last_name':'tiburcio'
    },
    2:{
        'first_name':'Rafael',
        'last_name':'Aguirre'
    },
    3:{
        'first_name':'Rodolfo',
        'last_name':'Bautista'
    },
    4:{
        'first_name':'Wilber',
        'last_name':'Villalvazo'
    },
    5:{
        'first_name':'Efrain',
        'last_name':'Ramirez'
    },
    6:{
        'first_name':'Oscar',
        'last_name':'Flores'
    },
    7:{
        'first_name':'Gerardo',
        'last_name':'Herrera'
    },
    8:{
        'first_name':'Nohemi',
        'last_name':'Bautista'
    },
    9:{
        'first_name':'Eduardo',
        'last_name':'Espinosa'
    },
    10:{
        'first_name':'Nelson',
        'last_name':'Lara'
    }
}

'''
Funcion para corregir los nombres de las actividades ya creadas
'''
def corrige_actividades():
    actividades = Activity.objects.all()
    for actividad in actividades:
        actividad.name = 'No planeadas CC-INX-{}'.format(
            actividad.project.cc_number
        )
        actividad.save()
'''
Funcion para agregar a la aplicación una lista de clientes desde
la lista de proyectos generada por airtable
'''
def crea_clientes():
    clients = df['CLIENTE'].unique()
    uppercase_clients = [str(client).upper() for client in clients]
    rt_clients = [str(client.name).upper() for client in Client.objects.all() ]
    for client in uppercase_clients:
        if client not in rt_clients:
            print('nuevo cliente')
            new_client = Client.objects.create(
                name = str(client)
            )
            new_client.save()

'''
Corrige proyectos no creados porque el cliente no tenia el mismo nombre
'''
def corrige_clientes_en_proyectos():
    print("Corrige clientes en proyectos...")
    project_id = []
    project_name = []
    for row in df.index:
        project = df['Centro de costos'][row]
        #print(project)
        if type(project) is str:
            #print("Project is str")
            project_name.append(project[project.find(' '):])
            if len(project) > 7:
                #print(project)
                project_sub = project[:7]
                if project_sub == 'CC-INX-':
                    project[project.find(' '):]
                    newstr = ''.join([n for n in project.split(' ')[0][7:] if n.isdigit()])
                    if newstr > '':
                        project_id.append(int(newstr))
                    else:
                        project_id.append(None)
                else:
                    project_id.append(None)
            else:
                project_id.append(None)
        else:
            project_id.append(None)
            project_name.append(None)
    df['CC']=project_id
    df['name']=project_name
    id_last = int(df.sort_values(by='CC', ascending=False).iloc[0]['CC'])
    df_sorted = df.sort_values(by='CC')
    proj_id = 1
    for proj_id in range(1,id_last+1):
        try:
            current_project = Project.objects.get(cc_number=proj_id)
            if not current_project.client:
                oldDbProject = df_sorted[df_sorted['CC'] == proj_id]
                if not oldDbProject.empty:
                    try:
                        client = Client.objects.get(name=oldDbProject.iloc[0]['CLIENTE'])
                        current_project.client = client
                        current_project.save()
                        print('si: {}.- {}'.format(
                            proj_id,
                            oldDbProject.iloc[0]['CLIENTE']
                        ))
                    except:
                        print('no: {}.- {}'.format(
                            proj_id,
                            oldDbProject.iloc[0]['CLIENTE']
                        ))
            #current_project.save()
        except:
            oldDbProject = df_sorted[df_sorted['CC'] == proj_id]
            if (not oldDbProject.empty):
                if len(oldDbProject) > 1:
                    print(oldDbProject)
                else:
                    exist = Project.objects.filter(cc_number=proj_id)
                    if(not exist):
                        print('sin datos: {}'.format(proj_id))
            else:
                print("no_project {}".format(proj_id))
'''
Corrige proyectos no creados porque el cliente no tenia el mismo nombre
'''
def corrige_proyectos():
    print("Corrige proyectos...")
    project_id = []
    project_name = []
    for row in df.index:
        project = df['Centro de costos'][row]
        #print(project)
        if type(project) is str:
            #print("Project is str")
            project_name.append(project[project.find(' '):])
            if len(project) > 7:
                #print(project)
                project_sub = project[:7]
                if project_sub == 'CC-INX-':
                    project[project.find(' '):]
                    newstr = ''.join([n for n in project.split(' ')[0][7:] if n.isdigit()])
                    if newstr > '':
                        project_id.append(int(newstr))
                    else:
                        project_id.append(None)
                else:
                    project_id.append(None)
            else:
                project_id.append(None)
        else:
            project_id.append(None)
            project_name.append(None)
    df['CC']=project_id
    df['name']=project_name
    id_last = int(df.sort_values(by='CC', ascending=False).iloc[0]['CC'])
    df_sorted = df.sort_values(by='CC')
    proj_id = 1
    for proj_id in range(1,id_last+1):
        try:
            current_project = Project.objects.get(pk=proj_id)
            current_project.cc_number = proj_id
            current_project.save()
        except:
            oldDbProject = df_sorted[df_sorted['CC'] == proj_id]
            if (not oldDbProject.empty):
                if len(oldDbProject) > 1:
                    print(oldDbProject)
                else:
                    exist = Project.objects.filter(cc_number=proj_id)
                    if(not exist):
                        new_project = Project.objects.create()
                        new_project.name = oldDbProject.iloc[0]['name']
                        if pd.notna(oldDbProject.iloc[0]['Tipo']):
                            new_project.type = tipos[oldDbProject.iloc[0]['Tipo']]
                        new_project.date = datetime.strptime(
                            oldDbProject.iloc[0]['Created'],
                            '%d/%m/%Y %I:%M%p'
                        )
                        if pd.notna(oldDbProject.iloc[0]['CLIENTE']):
                            try:
                                client = Client.objects.get(
                                    name=oldDbProject.iloc[0]['CLIENTE'].upper()
                                )
                                new_project.client = client
                            except:
                                print('client not exist: {}'.format(
                                    oldDbProject.iloc[0]['CLIENTE']
                                ))
                        if pd.notna(oldDbProject.iloc[0]['Responsable']):
                            try:
                                new_project.manager = User.objects.get(
                                    pk = usuarios[oldDbProject.iloc[0]['Responsable']]
                                )
                            except:
                                print('user not exist: {}'.format(
                                    oldDbProject.iloc[0]['Responsable']
                                ))
                        if pd.notna(oldDbProject.iloc[0]['Estado']):
                            new_project.status = estados[oldDbProject.iloc[0]['Estado']]
                        new_project.cc_number = proj_id
                        new_project.save()
                        if new_project.status != 0 or new_project.type == 1:
                            Activity.objects.filter(project=new_project).delete()
            else:
                print("no_project {}".format(proj_id))
'''
Funcion para agregar proyectos
'''
def crea_proyectos():
    print("Crea proyectos...")
    project_id = []
    project_name = []
    for row in df.index:
        project = df['Centro de costos'][row]
        #print(project)
        if type(project) is str:
            #print("Project is str")
            project_name.append(project[project.find(' '):])
            if len(project) > 7:
                #print(project)
                project_sub = project[:7]
                if project_sub == 'CC-INX-':
                    project[project.find(' '):]
                    newstr = ''.join([n for n in project.split(' ')[0][7:] if n.isdigit()])
                    if newstr > '':
                        project_id.append(int(newstr))
                    else:
                        project_id.append(None)
                else:
                    project_id.append(None)
            else:
                project_id.append(None)
        else:
            project_id.append(None)
            project_name.append(None)
    df['CC']=project_id
    df['name']=project_name
    #print(df)
    id_last = int(df.sort_values(by='CC', ascending=False).iloc[0]['CC'])
    #print(id_last)
    df_sorted = df.sort_values(by='CC')
    #print(df_sorted)
    # strdate = df_sorted.iloc[0]['Created']
    # date = datetime.strptime(strdate, '%d/%m/%Y %I:%M%p')
    # types = df['Responsable'].unique()
    # print(types)
    # print(date)
    proj_id = 1
    while proj_id <= id_last:
        new_project = Project.objects.create()
        #print(new_project)
        proj_id = new_project.pk
        #print(proj_id)
        try:
            newproject = df_sorted[df_sorted['CC'] == proj_id]
            #print(newproject.iloc[0])
            new_project.name = newproject.iloc[0]['name']
            if pd.notna(newproject.iloc[0]['Tipo']):
                new_project.type = tipos[newproject.iloc[0]['Tipo']]
            new_project.date = datetime.strptime(
                newproject.iloc[0]['Created'],
                '%d/%m/%Y %I:%M%p'
            )
            if pd.notna(newproject.iloc[0]['CLIENTE']):
                new_project.client = Client.objects.get(
                    name=newproject.iloc[0]['CLIENTE'].upper()
                )
            if pd.notna(newproject.iloc[0]['Responsable']):
                new_project.manager = User.objects.get(
                    pk = usuarios[newproject.iloc[0]['Responsable']]
                )
            if pd.notna(newproject.iloc[0]['Estado']):
                new_project.status = estados[newproject.iloc[0]['Estado']]
            new_project.save()
            if new_project.status != 0 or new_project.type == 1:
                Activity.objects.filter(project=new_project).delete()
        except:
            print("Proyecto no encontrado")
            new_project.delete()
    # new_project = Project.objects.create()
    # newproject = df_sorted[df_sorted['CC'] == new_project.pk]
    # new_project.name = newproject.iloc[0]['name']
    # new_project.save()
    # for row in df_sorted.index:
    #     if df['CC'][row] > 0:
    #         print(df_sorted['name'][row])
    #         #print(df['Centro de costos'][row])
'''
Crea un archivo csv con una lista de proyectos que no se
pueden crear automaticamente
'''
def no_creados():
    project_id = []
    project_name = []
    for row in df.index:
        project = df['Centro de costos'][row]
        if type(project) is str:
            project_name.append(project[project.find(' '):])
            if len(project) > 7:
                project_sub = project[:7]
                if project_sub == 'CC-INX-':
                    project[project.find(' '):]
                    newstr = ''.join([n for n in project.split(' ')[0][7:] if n.isdigit()])
                    if newstr > '':
                        project_id.append(int(newstr))
                    else:
                        project_id.append(None)
                else:
                    project_id.append(None)
            else:
                project_id.append(None)
        else:
            project_id.append(None)
            project_name.append(None)
    df['CC']=project_id
    df['name']=project_name
    id_last = int(df.sort_values(by='CC', ascending=False).iloc[0]['CC'])
    df['Created'] = pd.to_datetime(df['Created'], format='%d/%m/%Y %I:%M%p')
    df_sorted = df.sort_values(by='Created')
    print(df_sorted[291:339])
'''
Crea a los usuarios
'''
def crea_usuarios():
    for i in range(1,11):
        print(current_users[i]['first_name'])
        try:
            u = User.objects.get(pk=i)
            print(u.username)
            u.first_name = current_users[i]['first_name']
            u.last_name = current_users[i]['last_name']
            u.save()
        except:
            newuser = User.objects.create(
                username = '{}_{}'.format(
                    current_users[i]['first_name'],
                    current_users[i]['last_name']
                ),
                first_name = current_users[i]['first_name'],
                last_name = current_users[i]['last_name']
            )
            workergroup = Group.objects.get(name='Trabajador')
            workergroup.user_set.add(newuser)

if __name__=='builtins':
    #corrige_actividades()
    #corrige_clientes_en_proyectos()
    #crea_usuarios()
    #crea_clientes()
    crea_proyectos()
    #no_creados()
    #corrige_proyectos()
else:
    print(__name__)
    types = df['Tipo'].unique()
    admon = df['Administracion'].unique()
    state = df['Estado'].unique()
    responsable = df['Responsable'].unique()
    projects = df['Centro de costos'].unique()

    project = projects[30][7:]
    
    print(project.split(' ')[0])
