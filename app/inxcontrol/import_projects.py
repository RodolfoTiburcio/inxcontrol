import pandas as pd
from redminelib import Redmine
from django.contrib.auth.models import User, Group
from django.core.files.uploadedfile import UploadedFile
from profiles.models import Client
from projects.models import Project, Activity, Report, Photo
from datetime import datetime

# Run this program with: exec(open("./inxcontrol/import_projects.py").read())
# in a container django shell

reports_df = pd.read_pickle('./inxcontrol/reports_df.pkl')
journals_df = pd.read_pickle('./inxcontrol/journals_df.pkl')
cc = 322

reports_workers = {
    'JGEV - JESUS G ESCAMILLA V':5,
    'JMC  - JOSE MIGUEL CERVANTES':6,
    'JAOL - JORGE A. ORTIZ':7,
    'RLC - BETO LEZAMA':8,
    'MARA - MIGUEL APARICIO':9,
    'AMPE - ANGEL PONCE':10,
    'JROS - JORGE R. SUAREZ':11,
    'JDC  - JOSE DAVID CERVANTES':12,
    'LAVT - ALFONSO VILLANUEVA': 13,
    'EEGA - EFREN':14,
    'WABB - WALTER':15,
    'HBLA - HUGO LIMA':16,
    'SPG - SERGIO PEREZ':17,
    'JRM - JOEL RAMIREZ':4,
}
report_manager = {
    'Raymundo O.':18,
    'Rafael A.':19,
}

    
def agrega_foto(df, df_id, report_id):
    try:
        report_to_add_pic = Report.objects.get(pk=report_id)
    except:
        report_to_add_pic = None
        print('reporte {} no encontrado'.format(report_id))
    if report_to_add_pic:
        redmine = Redmine('http://t.inamexa.com:8082', username = 'adrian.islas@inamexa.com', password='testpython983742378659384')
        for item in df.iloc[df_id]['fotos']:
            if item['property'] == 'attachment':
                attachment = redmine.attachment.get(item['name'])
                filepath = attachment.download(
                    savepath = './mediafiles/redmine/',
                    filename = item['new_value']
                )
                if filepath:
                    photo = Photo.objects.create(
                        report = report_to_add_pic,
                    )
                    photo.pictures.save(
                        item['new_value'],
                        UploadedFile(
                            file=open(
                                filepath,
                                'rb'
                            ),
                        content_type='image/png',
                        )
                    )

def get_workers_name():
    single_work = []
    for workerday in reports_df['workers']:
        for each in workerday:
            single_work.append(each)
    workers = set(single_work)
    for worker in workers:
        print(worker)

def crea_reporte(df, df_id, planeadas, no_planeadas, comment = False):
    report = df.iloc[df_id]
    print(report)
    new_report = Report()
    try:
        new_report.responsable = User.objects.get(
            pk=report_manager[report['responsable']]
        )
    except:
        print('usuario "{}" no encontrado.'.format(
            report['responsable']
        ))
    new_report.date = report['fecha']
    if(not comment):
        if len(report['descripcion']) < 80:
            new_report.name = report['descripcion']
        else:
            new_report.name = report['descripcion'][0:79]
            new_report.description = report['descripcion']
        try:
            if report['tipo_de_actividad'] == 'Proyecto':
                act = Activity.objects.get(pk = planeadas)
            else:
                act = Activity.objects.get(pk = no_planeadas)
            new_report.activity = act
        except:
            print('Actividad {no_planeadas} o {planeadas} no encontrada'.format(
                no_planeadas, planeadas
        ))
        new_report.duration = report['tiempo_dedicado']
        new_report.deadtime = report['deadtime']
    else:
        if len(report['comment']) < 80:
            new_report.name = report['comment']
        else:
            new_report.name = report['comment'][0:79]
            new_report.description = report['comment']
        new_report.activity = Activity.objects.get(pk = planeadas)
    new_report.save()
    if comment:
        agrega_foto(df = df, df_id = df_id, report_id = new_report.pk)
    else:
        for worker in report['workers']:
            w = User.objects.get(pk = reports_workers[worker])
            new_report.workers.add(w)

def crea_reportes(df, is_comment):
    for i in range(len(df)):
        crea_reporte(
            df,
            df_id = i,
            planeadas=3,
            no_planeadas=4,
            comment = is_comment
        )

    

if __name__=='builtins':
    #agrega_foto(df_id = 0, report_id = 36)
    #get_workers_name()
    crea_reportes(journals_df, True)
    crea_reportes(reports_df, False)
          
else:
    print(__name__)