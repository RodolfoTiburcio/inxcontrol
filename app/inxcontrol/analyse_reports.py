import pandas as pd
import datetime

"""
exec(open("./inxcontrol/analyse_reports.py").read())
"""


fecha = datetime.date(2023,8,1)
reports_df = pd.read_pickle('./inxcontrol/reports_df.pkl')
journals_df = pd.read_pickle('./inxcontrol/journals_df.pkl')
# print('Reportes:')
# print(reports_df.loc[reports_df['fecha'] == fecha])
# print('Notas:')
# print(journals_df.loc[journals_df['fecha'] == fecha])

print(reports_df)
print(journals_df)