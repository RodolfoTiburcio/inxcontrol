window.onload = function() {
    let in_new_offer = document.getElementById("id_new_offer_time");
    let in_offer_followup = document.getElementById("id_offer_followup_time");
    let in_visit = document.getElementById("id_visit_time");
    let in_visit_followup = document.getElementById("id_visit_followup_time");
    let in_register = document.getElementById("id_register_time");
    let in_tender_portal = document.getElementById("id_tender_portal_time");
    let in_other = document.getElementById("id_other_time");
    in_new_offer.style.display = "none";
    in_offer_followup.style.display = "none";
    in_visit.style.display = "none";
    in_visit_followup.style.display = "none";
    in_register.style.display = "none";
    in_tender_portal.style.display = "none";
    in_other.style.display = "none";
}
function newOfferChange() {
    let in_new_offer = document.getElementById("id_new_offer_time");
    let chbox = document.getElementById("id_new_offer");
    if (chbox.checked) {
        in_new_offer.style.display = "inline-block";
    } else {
        in_new_offer.style.display = "none";
    }
}
function followUpOfferChange() {
    let in_offer_followup = document.getElementById("id_offer_followup_time");
    let chbox = document.getElementById("id_offer_followup");
    if (chbox.checked) {
        in_offer_followup.style.display = "inline-block";
    } else {
        in_offer_followup.style.display = "none";
    }
}
function newVisitChange() {
    let in_visit = document.getElementById("id_visit_time");
    let chbox = document.getElementById("id_new_visit");
    if (chbox.checked) {
        in_visit.style.display = "inline-block";
    } else {
        in_visit.style.display = "none";
    }
}
function visitFollowUpChange() {
    let in_visit_followup = document.getElementById("id_visit_followup_time");
    let chbox = document.getElementById("id_visit_followup");
    if (chbox.checked) {
        in_visit_followup.style.display = "inline-block";
    } else {
        in_visit_followup.style.display = "none";
    }
}
function registerChange() {
    let in_register = document.getElementById("id_register_time");
    let chbox = document.getElementById("id_register");
    if (chbox.checked) {
        in_register.style.display = "inline-block";
    } else {
        in_register.style.display = "none";
    }
}
function tenderPortalChange() {
    let in_tender_portal = document.getElementById("id_tender_portal_time");
    let chbox = document.getElementById("id_tender_portal");
    if (chbox.checked) {
        in_tender_portal.style.display = "inline-block";
    } else {
        in_tender_portal.style.display = "none";
    }
}
function otherChange() {
    let in_other = document.getElementById("id_other_time");
    let chbox = document.getElementById("id_other");
    if (chbox.checked) {
        in_other.style.display = "inline-block";
    } else {
        in_other.style.display = "none";
    }
}