from django.db import models
from django.contrib.auth.models import User

from profiles.models import Client, ClientContact
from projects.models import Activity

from datetime import datetime
    
class Report(models.Model):
    responsable = models.ForeignKey(
        User,
        related_name='%(class)s_seller',
        on_delete=models.SET_NULL,
        null = True,
        blank = True,
        help_text = "Responsable de la actividad")
    date = models.DateField(
        default = datetime.now,
        help_text = "Fecha de la actividad AAAA-MM-DD"
    )
    name = models.CharField(
        max_length = 80,
        null=True,
        blank=True,
        help_text = "Nombre corto del reporte"
    )
    description = models.TextField(
        max_length=4096,
        help_text = "Descripción",
        null = True,
        blank = True)
    
    def __str__(self):
        if self.name:
            return self.name
        else:
            return  self.date.strftime("%d/%m/%Y")

class TimeEntry(models.Model):
    class EntryType(models.IntegerChoices):
        NEWOFFER = 0, ('Nueva oferta')
        OFFERFOLLOWUP = 1, ('Seguimiento a oferta')
        VISIT = 2, ('Visita a cliente')
        VISITFOLLOWUP = 3, ('Seguimiento a visita')
        REGISTER = 4, ('Registro en plataforma')
        TENDERPORTAL = 5, ('Portal de licitaciones')
        # . . .  espacio para mas tipo de actividades
        OTHER = 9, ('Actividades no previstas')

    report = models.ForeignKey(
        Report,
        on_delete=models.CASCADE,
        blank=True,
        null = True,
        help_text = "Reporte correspondiente",
    )
    entry_type = models.IntegerField(
        choices = EntryType.choices,
        help_text = 'Tipo de actividad'
    )
    entry_time = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
        help_text='Tiempo dedicado a esta actividad'
    )

class Opportunity(models.Model):
    class OpStatus(models.IntegerChoices):
        NEW = 0, ('Levantamiento')
        TECHNICAL = 1, ('Tecnica')
        ECONOMIC = 2, ('Economica')
        SEND = 3, ('Enviada')
        WIN = 4, ('Ganada')
        LOSST  = 5, ('Perdido')
        # . . .  espacio para mas estados
        OTHER = 9, ('Otro')
    class currency(models.IntegerChoices):
        MXN = 0, ('MXN')
        USD = 1, ('USD')
        EUR = 2, ('EUR')
        # . . .  espacio para mas monedas
        OTHER = 9, ('Otra')
    
    name = models.CharField(
        max_length = 80,
        null=True,
        blank=True,
        help_text = "Nombre corto de la oportunidad"
    )
    responsable = models.ForeignKey(
        User,
        related_name='%(class)s_seller',
        on_delete=models.SET_NULL,
        null = True,
        blank = True,
        help_text = "Responsable de la oportunidad")
    status = models.IntegerField(
        choices = OpStatus.choices,
        default=0,
        help_text = 'Estado de la oportunidad'
    )
    open_date = models.DateField(
        default = datetime.now,
        blank=True,
        null=True,
        help_text = "Fecha de apertura AAAA-MM-DD"
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        blank=True,
        null = True,
        help_text = "Cliente",
    )
    contacts = models.ManyToManyField(
        ClientContact,
        blank=True,
        help_text = "Contactos",
    )
    description = models.TextField(
        max_length=4096,
        help_text = "Descripción",
        null = True,
        blank = True)
    amount = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        blank=True,
        null=True,
        help_text='Monto de la oportunidad'
    )
    currency = models.SmallIntegerField(
        choices=currency.choices,
        default=0,
        blank=True,
        null=True,
        help_text='Moneda de la oportunidad'
    )
    win_probability = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        blank=True,
        null=True,
        default=15.00,
        help_text='Probabilidad de ganar la oportunidad'
    )
    close_date = models.DateField(
        blank=True,
        null=True,
        help_text = "Fecha de cierre AAAA-MM-DD"
    )
    close_reason = models.TextField(
        max_length=4096,
        help_text = "Razón de cierre",
        null = True,
        blank = True)

    def __str__(self):
        if self.client:
            return self