from typing import Any
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .forms import ReportCreate, OpportunityCreate, OpportunityUpdate
from .models import Report, TimeEntry, Opportunity

from datetime import datetime

class ReportCreateView(LoginRequiredMixin, FormView):
    form_class = ReportCreate
    template_name = 'sales/report_form.html'
    success_url = reverse_lazy('sales:report_list')

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs =  super().get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'responsable':u.pk,
            'date':datetime.now(),
        }
        return kwargs
    
    def form_valid(self, form):
        new_report = Report.objects.create(
            responsable = form.cleaned_data['responsable'],
            date = form.cleaned_data['date'],
            name = form.cleaned_data['name'],
            description = form.cleaned_data['description']
        )
        if form.cleaned_data['new_offer']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['new_offer_time'],
                entry_type = 0,
        )
        if form.cleaned_data['offer_followup']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['offer_followup_time'],
                entry_type = 1,
        )
        if form.cleaned_data['new_visit']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['visit_time'],
                entry_type = 2,
        )
        if form.cleaned_data['visit_followup']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['visit_followup_time'],
                entry_type = 3,
        )
        if form.cleaned_data['register']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['register_time'],
                entry_type = 4,
        )
        if form.cleaned_data['tender_portal']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['tender_portal_time'],
                entry_type = 5,
        )
        if form.cleaned_data['other']:
            new_time_entry = TimeEntry.objects.create(
                report = new_report,
                entry_time = form.cleaned_data['other_time'],
                entry_type = 9,
        )
        return super().form_valid(form)

class ReportListView(LoginRequiredMixin, ListView):
    model = Report
    paginate_by = 20
    context_object_name = 'report_list'

class ReportDetailView(LoginRequiredMixin, DetailView):
    model = Report
    context_object_name='report'

class ReportDeleteView(LoginRequiredMixin, DeleteView):
    model = Report
    success_url = reverse_lazy('sales:report_list')
    context_object_name='report'

class OpportunityCreateView(LoginRequiredMixin, CreateView):
    model = Opportunity
    form_class = OpportunityCreate
    success_url = reverse_lazy('sales:opportunity_list')

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs =  super().get_form_kwargs()
        u = self.request.user
        kwargs['initial'] = {
            'responsable':u.pk,
            'date':datetime.now(),
        }
        return kwargs

class OpportunityListView(LoginRequiredMixin, ListView):
    model = Opportunity
    paginate_by = 20
    context_object_name = 'opportunity_list'

class OpportunityDetailView(LoginRequiredMixin, DetailView):
    model = Opportunity
    context_object_name='opportunity'

class OpportunityUpdateView(LoginRequiredMixin, UpdateView):
    model = Opportunity
    form_class = OpportunityUpdate
    success_url = reverse_lazy('sales:opportunity_list')

    

class OpportunityDeleteView(LoginRequiredMixin, DeleteView):
    model = Opportunity
    success_url = reverse_lazy('sales:opportunity_list')
    context_object_name='opportunity_list'

class OpportunityTableView(LoginRequiredMixin, ListView):
    model = Opportunity
    template_name = 'sales/opportunity_table.html'
    context_object_name = 'opportunity_list'

    def get_context_data(self, *args, **kwargs):
        context = super(OpportunityTableView, self).get_context_data(*args, **kwargs)
        context['offerts_array'] = [
            {'status':'LEVANTAMIENTO','offerts': Opportunity.objects.filter(status=0).order_by('-pk')},
            {'status':'TECNICA','offerts': Opportunity.objects.filter(status=1).order_by('-pk')},
            {'status':'ECONOMICA','offerts': Opportunity.objects.filter(status=2).order_by('-pk')},
            {'status':'ENVIADA','offerts': Opportunity.objects.filter(status=3).order_by('-pk')},
            {'status':'GANADA','offerts': Opportunity.objects.filter(status=4).order_by('-pk')},
        ]
        return context



@login_required
def update_status(request, pk, new_status):
    offert = Opportunity.objects.get(pk=pk)
    offert.status = new_status
    offert.save()
    return redirect('sales:opportunity_table')