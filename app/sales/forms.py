from django import forms
from django.forms import ModelChoiceField
from django.contrib.auth.models import User

from .models import TimeEntry, Opportunity, Activity

class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()
    
class ReportCreate(forms.Form):
    responsable = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget = forms.Select(),
        label = 'Responsable',
        help_text = 'Quien realiza el reporte',
    )
    date = forms.DateField(
        #initial = '',
        required = True,
        label = 'Fecha',
        help_text='Cuando se realiza el reporte',
        widget = forms.DateInput(attrs={
            'class':'datepicker form-control',
            'type':'date',
        }),
    )
    name = forms.CharField(
        max_length=80,
        required = False, 
        label = 'Nombre',
        help_text='Descripción corta del reporte',
    )
    description = forms.CharField(
        max_length=4096,
        required=False,
        label='Descripción',
        help_text = 'Detalle de las actividades',
        widget = forms.Textarea()
    )

    # Subform to add time entries
    # TIPOS DE ACTIVIDAD:
    # --- Nueva oferta ---
    new_offer = forms.BooleanField(
        required = False,
        label = 'Nueva oferta',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'newOfferChange()',
            }
        )
    )
    new_offer_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- Seguimiento oferta ---
    offer_followup = forms.BooleanField(
        required = False,
        label = 'Seguimiento de oferta',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'followUpOfferChange()',
            }
        )
    )
    offer_followup_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- Nueva visita ---
    new_visit = forms.BooleanField(
        required = False,
        label = 'Visita',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'newVisitChange()',
            }
        )
    )
    visit_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- Seguimiento a visita ---
    visit_followup = forms.BooleanField(
        required = False,
        label = 'Seguimiento de visita',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'visitFollowUpChange()',
            }
        )
    )
    visit_followup_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- Registro en plataforma ---
    register = forms.BooleanField(
        required = False,
        label = 'Registro en plataforma',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'registerChange()',
            }
        )
    )
    register_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- seguimiento a portales de licitaciones ---
    tender_portal = forms.BooleanField(
        required = False,
        label = 'Portales de licitación',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'tenderPortalChange()',
            }
        )
    )
    tender_portal_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )
    # --- Otras actividades ---
    other = forms.BooleanField(
        required = False,
        label = 'Otras actividades',
        initial = False,
        widget = forms.CheckboxInput(
            attrs={
                'onchange':'otherChange()',
            }
        )
    )
    other_time = forms.DecimalField(
        max_digits=4,
        decimal_places=2,
        required=False,
        label = '',
        widget = forms.NumberInput(attrs={
            'placeholder':'Tiempo dedicado con punto decimal (1.5 = 1:30)'
        })
    )

class OpportunityCreate(forms.ModelForm):
    responsable = UserModelChoiceField(
        queryset=User.objects.filter(
            is_active=True).order_by
            ('last_name'),
    )
    class Meta:
        model = Opportunity
        exclude = [
            'close_date',
            'close_reason',
        ]
        labels = {
            'name':'Nombre',
            'responsable':'Responsable',
            'status':'Estado',
            'open_date':'Fecha de apertura',
            'client':'Cliente',
            'contacts':'Contactos',
            'description':'Descripción',
            'amount':'Monto',
            'currency':'Moneda',
            'win_probability':'Probabilidad de ganar',
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Nombre corto de la oportunidad',
            }),
            'responsable': forms.Select(attrs={
                'class':'form-control',
            }),
            'status': forms.Select(attrs={
                'class':'form-control',
            }),
            'open_date': forms.DateInput(attrs={
                'class':'form-control',
                'type':'date',
            }),
            'client': forms.Select(attrs={
                'class':'form-control',
            }),
            'contacts': forms.SelectMultiple(attrs={
                'class':'form-control',
            }),
            'description': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'Descripción de la oportunidad',
            }),
            'amount': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Monto de la oportunidad',
            }),
            'currency': forms.RadioSelect(), 
            'win_probability': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Probabilidad de ganar',
            }),
        }

class OpportunityUpdate(forms.ModelForm):
    responsable = UserModelChoiceField(
        queryset=User.objects.filter(
            is_active=True).order_by
            ('last_name'),
    )
    class Meta:
        model = Opportunity
        fields = '__all__'
        labels = {
            'name':'Nombre',
            'responsable':'Responsable',
            'status':'Estado',
            'open_date':'Fecha de apertura',
            'client':'Cliente',
            'contacts':'Contactos',
            'description':'Descripción',
            'amount':'Monto',
            'currency':'Moneda',
            'win_probability':'Probabilidad de ganar',
            'close_date':'Fecha de cierre',
            'close_reason':'Razón de cierre',
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Nombre corto de la oportunidad',
            }),
            'responsable': forms.Select(attrs={
                'class':'form-control',
            }),
            'status': forms.Select(attrs={
                'class':'form-control',
            }),
            'open_date': forms.DateInput(attrs={
                'class':'form-control',
                'type':'date',
            }),
            'client': forms.Select(attrs={
                'class':'form-control',
            }),
            'contacts': forms.SelectMultiple(attrs={
                'class':'form-control',
            }),
            'description': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'Descripción de la oportunidad',
            }),
            'amount': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Monto de la oportunidad',
            }),
            'currency': forms.RadioSelect(), 
            'win_probability': forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Probabilidad de ganar',
            }),
            'close_date': forms.DateInput(attrs={
                'class':'form-control',
                'type':'date',
            }),
            'close_reason': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'Razón de cierre',
            }),
        }