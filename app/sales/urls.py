from django.urls import path, include
from django.contrib.auth.views import LoginView

from .views import (ReportCreateView, ReportListView, ReportDetailView,
                    ReportDeleteView, OpportunityCreateView, OpportunityListView,
                    OpportunityDetailView, OpportunityDeleteView, update_status,
                    OpportunityTableView, OpportunityUpdateView
                    )

app_name = 'sales'

urlpatterns = [
    path('', ReportListView.as_view(), name='report_list'),
    path('reportcreate', ReportCreateView.as_view(), name='report_create'),
    path('report/<int:pk>', ReportDetailView.as_view(), name='report_detail'),
    path('report/<int:pk>/delete', ReportDeleteView.as_view(), name='report_delete'),
    path('opportunitycreate', OpportunityCreateView.as_view(), name='opportunity_create'),
    path('opportunitylist', OpportunityListView.as_view(), name='opportunity_list'),
    path('opportunitytable', OpportunityTableView.as_view(), name='opportunity_table'),
    path('opportunity/<int:pk>/update', OpportunityUpdateView.as_view(), name='opportunity_update'),
    path('opportunity/<int:pk>', OpportunityDetailView.as_view(), name='opportunity_detail'),
    path('opportunity/<int:pk>/delete', OpportunityDeleteView.as_view(), name='opportunity_delete'),
    path('update_status/<int:pk>/<int:new_status>', update_status, name='status_update')
]