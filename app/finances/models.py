from django.db import models
from django.contrib.auth.models import User
from profiles.models import Supplier
from projects.models import Activity
from datetime import datetime

import os

class Movement(models.Model):
    payee = models.ForeignKey(
        Supplier,
        models.SET_NULL,
        blank = True,
        null = True
    )
    creation_date = models.DateField(
        default = datetime.now,
    )
    description = models.CharField(
        max_length=254
    )
    amount = models.DecimalField(
        decimal_places=3,
        max_digits=7
    )
    no_invoice = models.BooleanField(
        blank = True,
        default = False
    )
    payment = models.BooleanField(
        blank = True,
        default = False
    )
    responsable = models.ForeignKey(
        User,
        models.CASCADE
    )
    files = models.FileField(
        upload_to="finances/%Y/%m/%d/",
        blank = True,
        null = True
    )

class Order(models.Model):
    class status(models.IntegerChoices):
        NEW = 1, ('Nueva')
        TO_BE_APPROVED = 2, ('Por aprobar')
        TO_BE_PAIED = 3, ('Por pagar')
        NOTIFY_PROVIDER = 4, ('Notificar al proveedor')
        WAITING_GOODS = 5, ('Esperando mercancias')
        COMPLETE = 6, ('Completado')
    name = models.CharField(
        max_length=254,
        help_text = "Nombre corto de la orden de compra"
    )
    description = models.TextField(
        max_length=1023,
        help_text="Descripción corta de la orden de compra",
        null=True,
        blank=True,
    )
    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        related_name = 'orders',
        null = True,
        blank = True,
        help_text = "Actividad"
    )
    supplier =  models.ForeignKey(
        Supplier,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="Seleccione proveedor, si no existe dejar en blanco y agregar despues"
    )
    creation_date = models.DateTimeField(
        auto_now = True,
        editable = False,
    )
    end_date = models.DateTimeField(
        auto_now = False,
        editable = True,
        null = True,
        blank = True,
        help_text="Fecha limite para colocar orden"
    )
    status = models.IntegerField(
        choices = status.choices,
        default = 1,
    )
    delivery_conditions = models.TextField(
        max_length=255,
        help_text="Condiciones de entrega del proveedor",
        null=True,
        blank=True,
    )
    delivery_time = models.CharField(
        max_length=30,
        help_text="Tiempo de entrega promedio del proveedor",
        null=True,
        blank=True,
    )
    payment_conditions = models.CharField(
        max_length=255,
        help_text="Condiciones de pago por defecto del proveedor",
        blank=True,
        null = True,
    )
    request_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name = '%(class)s_request',
        null=True,
        blank=True,
        help_text="Quien solicita esta OC"
    )
    approved_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name = '%(class)s_approved',
        null=True,
        blank=True,
        help_text="Quien aprueba esta OC"
    )
    authorized_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name = '%(class)s_authorized',
        null=True,
        blank=True,
        help_text="Quien autoriza esta OC"
    )
    paid_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name = '%(class)s_paid',
        null=True,
        blank=True,
        help_text="Quien paga esta OC"
    )
    total = models.DecimalField(
        max_digits=10,
        decimal_places=3,
        blank=True,
        null=True,
        help_text = "Monto total e la OC"
    )

    def __str__(self):
        return str(self.pk)