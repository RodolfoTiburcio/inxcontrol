# Generated by Django 4.2.4 on 2023-08-24 03:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0002_alter_movement_files'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movement',
            name='files',
            field=models.FileField(blank=True, null=True, upload_to='finances/%Y/%m/%d/'),
        ),
    ]
