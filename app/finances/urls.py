from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from finances.views import (MovementCreateView, MovementUpdateView, MovementDeleteView,
                            MovementMontArchiveView,MovementLastMonth ,MovementAdminListView,
                            WorkersTimeListView, AttendanceReport, AttendanceReportFormClass,
                            AttendaceFullReportClass, UserAccountingTemplateView,
                            ProjectAttendanceReport, OrderCreateView, OrderListView,
                            OrderDetailView, OrderUpdateView, order_complete)

app_name = 'finances'

urlpatterns = [
    path('', MovementCreateView.as_view(), name = 'finances_home'),
    path('currentmonth',MovementLastMonth.as_view(), name='finances_lastmonth'),
    path('movementlist/<int:year>/<int:month>', MovementMontArchiveView.as_view(), name='finances_month'),
    path('movementresume', UserAccountingTemplateView.as_view(), name='finances_resume'),
    path('edit/<int:pk>', MovementUpdateView.as_view(), name = 'finances_edit'),
    path('delete/<int:pk>', MovementDeleteView.as_view(), name='finances_delete'),
    path('workerstime', WorkersTimeListView.as_view(), name='workerstime_list'),
    #path('attendance', AttendanceReport.as_view(), name='attendance_report'),
    path(
        'attendance/<str:inicio>/<str:final>/<str:extra>/<str:adicional>/<str:cliente>',
        AttendaceFullReportClass.as_view(),
        name='attendance_report'),
    path(
        # Formulario para generar reporte de asistencia
        'attendanceform',
        AttendanceReportFormClass.as_view(),
        name='attendance_form',
    ),
    path(
        'projectattendance/<int:pk>',
        ProjectAttendanceReport.as_view(),
        name='projectattendance_report'
    ),
    # path(
    #     'projectattendance/<int:pk>',
    #     permission_required('finances.change_movement')(ProjectAttendanceReport.as_view()),
    #     name = 'projectattendance_report'
    # ),
    path(
        'ordercreate',
        OrderCreateView.as_view(),
        name='order_create',
    ),
    path(
        'orderlist',
        OrderListView.as_view(),
        name = 'order_list',
    ),
    path(
        'orderdetail/<int:pk>',
        OrderDetailView.as_view(),
        name = 'order_detail',
    ),
    path(
        'orderupdate/<int:pk>',
        OrderUpdateView.as_view(),
        name = 'order_update',
    ),
    path(
        'ordercomplete/<int:pk>',
        order_complete,
        name = 'order_complete',
    )
]