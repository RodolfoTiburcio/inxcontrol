from django import forms
from django.forms import ModelChoiceField
from django.contrib.auth.models import User
from profiles.models import Supplier, Client
from datetime import datetime
from finances.models import Movement, Order
from projects.models import Activity

class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()

class MovementCreateForm(forms.ModelForm):
    responsable = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True).order_by('last_name'),
        widget = forms.Select(),
        label = 'Responsable',
    )
    class Meta:
        model = Movement
        fields = '__all__'
        widgets = {
            'payee':forms.Select(attrs={'class':'form-control'}),
            'creation_date':forms.DateInput(attrs={'class':'form-control datepicker','type':'date'}),
            'description':forms.Textarea(),
            'amount':forms.NumberInput(attrs={'class':'form-control'}),
            'no_invoice':forms.CheckboxInput(attrs={'class':'form-control'}),
            'payment':forms.CheckboxInput(attrs={'class':'form-control'}),
            'responsable':forms.Select(),
            'files':forms.FileInput(attrs={}),
        }
        labels = {
            'payee':'Proveedor',
            'creation_date':'Fecha de compra',
            'description':'Descripción',
            'amount':'Monto',
            'no_invoice':'Sin factura',
            'payment':'Pagado',
            'files':'Factura',
        }

class DateSelectForm(forms.Form):
    init_date = forms.CharField(
        label = 'Fecha de inicio: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    end_date = forms.CharField(
        label = 'Fecha final: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    
class AttendanceForm(forms.Form):
    init_date = forms.CharField(
        label = 'Fecha de inicio: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    end_date = forms.CharField(
        label = 'Fecha final: ',
        widget = forms.DateInput(attrs={'class':'form-control datepicker','type':'date'})
    )
    extra_time = forms.BooleanField(
        label = 'Solo tiempo extra',
        widget = forms.NullBooleanSelect(),
        initial = False,
        required = False,
    )
    adicionales = forms.BooleanField(
        label = 'Solo actividades adicionales',
        widget = forms.NullBooleanSelect(),
        initial = False,
        required=False,
    )
    cliente = forms.ModelChoiceField(
        queryset = Client.objects.all().order_by('name'),
        label = 'Cliente',
        widget = forms.Select(),
        initial=False,
        required = False,
    )

class OrderCreateForm(forms.ModelForm):
    request_by = UserModelChoiceField(
        queryset=User.objects.filter(is_active=True),
        widget = forms.Select(),
        label = 'Solicitado por',
    )
    activity = forms.ModelChoiceField(
        queryset=Activity.objects.filter(status__lte=1).order_by('project'),
        required=False,
        label = 'Actividad relacionada'
    )
    supplier = forms.ModelChoiceField(
        queryset=Supplier.objects.all().order_by('name'),
        required=False,
        label="Proveedor"
    )

    class Meta:
        model = Order
        fields = ['name', 'description', 'activity','supplier',
            'status', 'request_by', 'total']
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control'}),
            'activity':forms.Select(attrs={'class':'form-select'}),
            'supplier':forms.Select(attrs={'class':'form-select'}),
            'creation_date':forms.DateInput(attrs={
                'class':'datepicker',
                'type':'date',
                'class':'form-control',
            }),
            'end_date':forms.DateInput(attrs={
                'class':'datepicker',
                'type':'date',
                'class':'form-control',
            }),
            'status':forms.Select(attrs={'class':'form-select'}),
            #'delivery_conditions':forms.Textarea(attrs={'class':'form-control'}),
            'delivery_time':forms.TextInput(attrs={'class':'form-control'}),
            #'payment_conditions':forms.Textarea(attrs={'class':'form-control'}),
            'request_by':forms.Select(attrs={'class':'form-select'}),
            'approved_by':forms.Select(attrs={'class':'form-select'}),
            'authorized_by':forms.Select(attrs={'class':'form-select'}),
            'paid_by':forms.Select(attrs={'class':'form-select'}),
            'total':forms.NumberInput(attrs={'class':'form-control'}),
        }
        labels = {
            'name':'Nombre',
            'description':'Descripción',
            'supplier':'Proveedor',
            'creation_date':'Fecha de creación',
            'end_date':'Fecha de cierre',
            'status':'Estado',
            'delivery_conditions':'Condiciones de entrega',
            'delivery_time':'Tiempo de entrega',
            'payment_conditions':'Condiciones de pago',
            'approved_by':'Aprovado por',
            'authorized_by':'Authorizado por',
            'paid_by':'Pagado por',
            'total':'Total de OC',
        }